import Vue from 'vue'
import noteApi from '@/api/notes';
// https://dev.to/f3ltron/vue-2-6-6-release-part3-vue-observable-21dk
// Pas d'appels dans les composants
export const state = Vue.observable({
  notes: [],
  note: {
    id: 0,
    title: "",
    text: "",
    archived: false,
    color: "#fff"
  },
  addNoteMsg: {
    message: "",
    timeout: 2000,
    actionText: "Undo"
  }
});
// Pas asynchrone
export const mutations = {
  setNotes(notes) {
    state.notes = notes;
  }
};

// Méthodes (promesses)
// TOujours modifier les datas dans les mutations
export const actions = {
  async getNotes() {
    try {
      const notes = await noteApi.get();
      mutations.setNotes(notes);
    } catch (e) {
      console.log(e);
    }
  }
}