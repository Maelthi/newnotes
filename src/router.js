import Vue from "vue";
import Router from "vue-router";
import Notes from "./views/Home.vue";
import Archives from "./views/Archives.vue";
import NavBar from "./views/NavBar.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "notes",
      components: {
        default: Notes,
        nav: NavBar
      }
    },
    {
      path: '/notes/:noteId',
      name: 'notesDetail',
      props: true,
      components: {
        nav: NavBar
      }
    },
    {
      path: "/archives",
      name: "archives",
      components: {
        default: Archives,
        nav: NavBar
      }
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
});
